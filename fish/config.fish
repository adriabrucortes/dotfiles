if status is-interactive
    # Commands to run in interactive sessions can go here
end

export PATH="/nix/var/nix/profiles/default/bin:$PATH"
export PATH="/home/adria/.nix-profile/bin:$PATH"
export PATH="/nix/var/nix/profiles/default/bin:$PATH"

set -U fish_greeting
starship init fish | source

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
if test -f /home/adria/miniconda3/bin/conda
    eval /home/adria/miniconda3/bin/conda "shell.fish" "hook" $argv | source
end
# <<< conda initialize <<<
# To show (env) in terminal:
# conda config --set changeps1 True

# Aliases
alias apt="sudo nala" # If I want to use apt, just do "sudo apt"
alias v="nvim"
alias r="ranger"
