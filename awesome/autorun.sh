#!/bin/sh

run() {
    if ! pgrep -f "$1"; then
        "$@" &
    fi
}

run "picom"
run "nitrogen" --restore
run "spotify"
run "discord"
run "flatpak" run io.github.mimbrero.WhatsAppDesktop
run "mailspring"
run "slack"
