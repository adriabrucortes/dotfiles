"""
Color schemes live here
"""

# #1e1e2e #cdd6f4

_gruvbox = {
    'bg':           '#000000',
    'fg':           '#ffffff',
    'dark-red':     '#a6e3a1',
    'red':          '#94e2d5',
    'dark-green':   '#89dceb',
    'green':        '#74c7ec',
    'dark-yellow':  '#89dceb',
    'yellow':       '#b4befe',
    'dark-blue':    '#f5e0dc',
    'blue':         '#74c7ec',
    'dark-magenta': '#f5c2e7',
    'magenta':      '#cba6f7',
    'dark-cyan':    '#f38ba8',
    'cyan':         '#eba0ac',
    'dark-gray':    '#ffffff',
    'gray':         '#74c7ec',

    'fg4':          '#766f64',
    'fg3':          '#665c54',
    'fg2':          '#504945',
    'fg1':          '#3c3836',
    'bg0':          '#32302f',
    'fg0':          '#1d2021',
    'fg9':          '#ebdbb2'
}

color_schema = _gruvbox
