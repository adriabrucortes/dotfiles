"""
Group definitions are here
"""
from libqtile.config import Group, Match

default_layout = "columns"

group_definitions = [
    {
        'name': 'general',
        'label': '󰀻',
        'layout': default_layout
    },
    {
        'name': 'web',
        'label': '󰈹',
        'matches': [Match(wm_class='firefox')],
        'layout': default_layout
    },
    {
        'name': 'files',
        'label': '',
        'layout': default_layout
    },
    {
        'name': 'dev',
        'label': '',
        'layout': default_layout
    },
    {
        'name': 'dev2',
        'label': '󰨞',
        'matches': [Match(wm_class='code')],
        'layout': default_layout
    },
    {
        'name': 'desktop1',
        'label': '󰇄',
        'matches': [Match(wm_class='teams-for-linux')],
        'layout': default_layout
    },
    {
        'name': 'spotify',
        'label': '󰓇',
        'matches': [Match(wm_class='Spotify')],
        'layout': default_layout
    },
    {
        'name': 'mail',
        'label': '󰛮',
        'layout': default_layout
    },
    {
        'name': 'social',
        'label': '󰵅',
        'layout': default_layout
    },
]

groups = [Group(**group) for group in group_definitions]
